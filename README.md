# 练习验证码
基本用法：

```
package main

import (
	"gitea.com/iwhot/capcha"
	"os"
)

func main() {
	f, _ := os.OpenFile("rgb.png", os.O_WRONLY|os.O_CREATE, 0600)
    defer f.Close()
    img := capcha.NewCapcha(f,150,30,100)
    img.Writer = f
    //生成验证码
    code := capcha.RandomText(4)
    img.GetCapcha(code,"png")
}
```
或者：
```
http.HandleFunc("/pic", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type","image/png")
		img := capcha.NewCapcha(w,150,30,100)
		//生成验证码
		code := capcha.RandomText(4)
		img.GetCapcha(code,"png")
	})
	log.Fatal(http.ListenAndServe(":8080",nil))
```