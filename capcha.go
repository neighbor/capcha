package capcha

import "C"
import (
	"errors"
	"flag"
	"github.com/golang/freetype"
	"golang.org/x/image/font"
	"image"
	"image/color"
	"image/draw"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"math/rand"
	"time"
)

const (
	IMGPNG  = "png"
	IMGGif  = "gif"
	IMGJPEG = "jpeg"
)

type capcha struct {
	Rgba    *image.RGBA //图片
	With    int         //宽
	Height  int         //高
	Comp    int         //噪点数量
	BgColor color.Color //背景
	Writer  io.Writer
	Code    string //验证码
}

var (
	//设置随机种子
	seed = rand.New(rand.NewSource(time.Now().UnixNano()))
	//分辨率
	dpi = flag.Float64("dpi", 72, "screen resolution in Dots Per Inch")
	//加载字体
	fontfile = flag.String("fontfile", "./font/rosewood.ttf", "filename of the ttf font")
)

//创建验证码对象
func NewCapcha(w io.Writer, with, height, comp int) *capcha {
	return &capcha{
		With:    with,
		Height:  height,
		Comp:    comp,
		BgColor: color.White,
		Writer:  w,
	}
}

func (c *capcha) GetCapcha(code, imgType string) error{
	img := image.NewRGBA(image.Rect(0, 0, c.With, c.Height))
	draw.Draw(img, img.Bounds(), &image.Uniform{c.BgColor}, image.ZP, draw.Src)
	c.Rgba = img
	c.Code = code
	return c.DrawComplex().DrawCode().SaveImage(c.Writer, imgType)
}

//生成图片
func (c *capcha) SaveImage(writer io.Writer, imgType string) error {
	if imgType == "png" {
		return png.Encode(writer, c.Rgba)
	} else if imgType == "gif" {
		return gif.Encode(writer, c.Rgba, &gif.Options{NumColors: 256})
	} else if imgType == "jpeg" {
		return jpeg.Encode(writer, c.Rgba, &jpeg.Options{100})
	}
	return errors.New("图片类型不存在")
}

//画噪点
func (c *capcha) DrawComplex() *capcha {

	size := c.Comp
	for i := 0; i < size; i++ {
		x := seed.Intn(c.With)
		y := seed.Intn(c.Height)
		if size%3 == 0 {
			c.Rgba.Set(x+1, y+1, RandomColor())
		}
	}
	return c
}

func (c *capcha) DrawCode() *capcha {
	cf := freetype.NewContext()
	cf.SetDPI(*dpi)             //设置分辨率
	cf.SetClip(c.Rgba.Bounds()) //设置背景
	cf.SetDst(c.Rgba)           //设置背景图
	cf.SetHinting(font.HintingFull)
	//获取验证码
	code := c.Code
	fontWidth := c.With / len(code)

	fontBytes, err := ioutil.ReadFile(*fontfile)
	if err != nil {
		panic(err)
	}
	f, err := freetype.ParseFont(fontBytes)
	if err != nil {
		panic(err)
	}
	//设置字体样式
	cf.SetFont(f)

	for i, s := range code {
		fontSize := float64(c.Height) / (1 + float64(seed.Intn(7))/float64(9))
		cf.SetSrc(image.NewUniform(RandDeepColor()))
		cf.SetFontSize(fontSize)

		//写入字体
		x := int(fontWidth)*i + int(fontWidth)/int(fontSize)
		y := 5 + seed.Intn(c.Height/2) + int(fontSize/2)
		pt := freetype.Pt(x, y)
		_, err := cf.DrawString(string(s), pt)
		if err != nil {
			panic(err)
		}
	}

	return c
}
