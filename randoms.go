package capcha

import (
	"image/color"
	"math"
	"math/rand"
	"strings"
	"time"
)

//随机颜色
func RandomColor() color.RGBA {
	seed := rand.New(rand.NewSource(time.Now().UnixNano()))
	red := seed.Intn(255)
	green := seed.Intn(255)
	blue := seed.Intn(255)
	return color.RGBA{
		R: uint8(red),
		G: uint8(green),
		B: uint8(blue),
		A: uint8(255),
	}
}

//生成随机验证码
func RandomText(num int) string {
	var str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	arr := strings.Split(str,"")
	seed := rand.New(rand.NewSource(time.Now().UnixNano()))
	l := len(arr)
	var s string
	for i := 0; i < num; i++ {
		r := seed.Intn((l - 1))
		s += arr[r]
	}
	return s
}

//随机生成深色系.
func RandDeepColor() color.RGBA {
	seed := rand.New(rand.NewSource(time.Now().UnixNano()))
	randColor := RandomColor() //随机生成颜色
	increase := float64(30 + seed.Intn(255))
	red := math.Abs(math.Min(float64(randColor.R)-increase, 255))
	green := math.Abs(math.Min(float64(randColor.G)-increase, 255))
	blue := math.Abs(math.Min(float64(randColor.B)-increase, 255))
	return color.RGBA{R: uint8(red), G: uint8(green), B: uint8(blue), A: uint8(255)}
}